
# Bank Kata for Exalt It

##  This project simulates a Bank use cases, using Spring Boot framework and reactive programming paradigm to handle complexity in the financial sector, I used Test Driven development and a Clean Hexagonal Architecture. 

### How to use the project? There is 3 Apis to consume:
* POST /api/v1/deposit to make deposits
* POST /api/v1/withdraw to withdraw
* GET  /api/v1/transactions to get all the operations

### Credits:
[Hexagonal Architecture](https://blog.octo.com/architecture-hexagonale-trois-principes-et-un-exemple-dimplementation/).

[SQL antipatterns](https://pragprog.com/titles/bksqla/sql-antipatterns/).

[project Reactor](https://projectreactor.io).




