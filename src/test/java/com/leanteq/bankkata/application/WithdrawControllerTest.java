package com.leanteq.bankkata.application;

import com.leanteq.bankkata.domain.model.Account;
import com.leanteq.bankkata.domain.model.StatusEnum;
import com.leanteq.bankkata.domain.model.WithdrawalStatus;
import com.leanteq.bankkata.domain.model.WithdrawalTransaction;
import com.leanteq.bankkata.domain.use_cases.WithdrawalService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Instant;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;

@WebFluxTest
class WithdrawControllerTest {
    @Autowired
    private WebTestClient client;

    @MockBean(WithdrawalService.class)
    private WithdrawalService withdrawalService;

    @Test
    void shouldWithdrawAmountFromAccountTest(){
        // When
        BigDecimal amount = BigDecimal.valueOf(100);
        WithdrawalStatus fakeWithdrawalStatus = new WithdrawalStatus(
                StatusEnum.SUCCESS,
                amount,
                Instant.now());
        Account account = new Account();
        Instant transactionTime = Instant.now();
        WithdrawalTransaction transaction = new WithdrawalTransaction(transactionTime, account, amount);
        when(withdrawalService.withdraw(transaction)).thenReturn(Mono.just(fakeWithdrawalStatus));

        // Act
        client.post().uri("/api/v1/withdraw")
                .bodyValue(transaction)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()

                // Assert
                .expectStatus().isOk()
                .expectBody(WithdrawalStatus.class)
                .value(response -> assertAll(
                                () -> assertThat(response.getWithdrawalAmount()).isNotNull(),
                                () -> assertThat(response.getStatusMessage()).isEqualTo(fakeWithdrawalStatus.getStatusMessage()),
                                () -> assertThat(response.getWithdrawalAmount()).isEqualTo(fakeWithdrawalStatus.getWithdrawalAmount())
                        )
                );

    }
}