package com.leanteq.bankkata.application;

import com.leanteq.bankkata.domain.model.*;
import com.leanteq.bankkata.domain.use_cases.DepositService;
import com.leanteq.bankkata.domain.use_cases.TransactionService;
import com.leanteq.bankkata.domain.use_cases.WithdrawalService;
import com.leanteq.bankkata.infrastructure.document.AccountJpa;
import com.leanteq.bankkata.infrastructure.document.DepositTransactionJpa;
import com.leanteq.bankkata.infrastructure.document.WithdrawalTransactionJpa;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

import static com.leanteq.bankkata.domain.model.StatusEnum.SUCCESS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class StatementControllerTest {

    @Autowired
    private WebTestClient client;

    @MockBean
    private TransactionService transactionService;

    @Test
    void shouldGetStatement() {
        Statement statementExpected = new Statement();
        DepositTransactionJpa transaction1 = new DepositTransactionJpa(Instant.now(), new AccountJpa(), BigDecimal.valueOf(100));
        WithdrawalTransactionJpa transaction2 = new WithdrawalTransactionJpa(Instant.now(), new AccountJpa(), BigDecimal.valueOf(100));
        statementExpected.setDepositTransactions(List.of(transaction1));
        statementExpected.setWithdrawalTransactions(List.of(transaction2));

        when(transactionService.getTransactions()).thenReturn(Mono.just(statementExpected));

        // Act
        client.get().uri("/api/v1/statement")
                .accept(MediaType.APPLICATION_JSON)
                .exchange()

                // Assert
                .expectStatus().isOk()
                .expectBody(Statement.class)
                .value(value -> value.equals(statementExpected));

    }
}