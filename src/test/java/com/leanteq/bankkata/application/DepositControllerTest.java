package com.leanteq.bankkata.application;

import com.leanteq.bankkata.domain.model.Account;
import com.leanteq.bankkata.domain.model.DepositStatus;
import com.leanteq.bankkata.domain.model.DepositTransaction;
import com.leanteq.bankkata.domain.use_cases.DepositService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Instant;

import static com.leanteq.bankkata.domain.model.StatusEnum.SUCCESS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.when;

/**
 * Deposit Controller class to unit test {@link DepositController} methods.
 *
 * @author Salah Eddine AIT YOUSSEF
 */

@WebFluxTest
public class DepositControllerTest {
    @Autowired
    private WebTestClient client;

    @MockBean
    private DepositService depositService;

    @Test
    void shouldDepositAmountToAccountTest(){
        BigDecimal amount = BigDecimal.valueOf(100);
        DepositStatus fakeDepositStatus = new DepositStatus(
                SUCCESS,
                amount,
                Instant.now());
        Account account = new Account();
        Instant transactionTime = Instant.now();
        DepositTransaction transaction = new DepositTransaction(transactionTime, account, amount);
        when(depositService.depositToSavingsAccount(transaction)).thenReturn(Mono.just(fakeDepositStatus));

        // Act
        client.post().uri("/api/v1/deposit")
                .bodyValue(transaction)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()

        // Assert
                .expectStatus().isOk()
                .expectBody(DepositStatus.class)
                .value(response -> assertAll(
                                () -> assertThat(response.getAmountDeposited()).isNotNull(),
                                () -> assertThat(response.getStatusMessage()).isEqualTo(fakeDepositStatus.getStatusMessage()),
                                () -> assertThat(response.getAmountDeposited()).isEqualTo(fakeDepositStatus.getAmountDeposited())
                        )
                );
    }
}
