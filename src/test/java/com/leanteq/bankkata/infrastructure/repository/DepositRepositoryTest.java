package com.leanteq.bankkata.infrastructure.repository;

import com.leanteq.bankkata.infrastructure.document.AccountJpa;
import com.leanteq.bankkata.infrastructure.document.DepositTransactionJpa;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit4.SpringRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.time.Instant;

@DataMongoTest
@RunWith(SpringRunner.class)
class DepositRepositoryTest {

    @Autowired
    DepositRepository depositRepository;

    @Test
    void shouldInsert() {
        // Given
        BigDecimal amount = BigDecimal.valueOf(100);
        Instant transactionTime = Instant.now();
        AccountJpa accountJpa = new AccountJpa(BigDecimal.valueOf(1000));
        DepositTransactionJpa transactionJpa = new DepositTransactionJpa(transactionTime, accountJpa, amount);

        // When
        Mono<DepositTransactionJpa> result = depositRepository.insert(transactionJpa);

        // Then
        StepVerifier.create(result)
                .expectNext(transactionJpa)
                .expectComplete()
                .verify();
    }
}