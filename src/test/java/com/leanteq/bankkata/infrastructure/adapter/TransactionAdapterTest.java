package com.leanteq.bankkata.infrastructure.adapter;

import com.leanteq.bankkata.domain.model.*;
import com.leanteq.bankkata.infrastructure.document.AccountJpa;
import com.leanteq.bankkata.infrastructure.document.CustomerJpa;
import com.leanteq.bankkata.infrastructure.document.DepositTransactionJpa;
import com.leanteq.bankkata.infrastructure.document.WithdrawalTransactionJpa;
import com.leanteq.bankkata.infrastructure.repository.AccountRepository;
import com.leanteq.bankkata.infrastructure.repository.CustomerRepository;
import com.leanteq.bankkata.infrastructure.repository.DepositRepository;
import com.leanteq.bankkata.infrastructure.repository.WithdrawalRepository;
import org.dozer.DozerBeanMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.matchers.Any;
import org.mockito.junit.MockitoJUnitRunner;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.time.Instant;

import static com.leanteq.bankkata.domain.model.StatusEnum.SUCCESS;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
class TransactionAdapterTest {

    private TransactionAdapter transactionAdapter;

    @Mock
    private DepositRepository depositRepository;

    @Mock
    private WithdrawalRepository withdrawalRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private DozerBeanMapper mapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        transactionAdapter = new TransactionAdapter( depositRepository, withdrawalRepository, customerRepository, accountRepository);
    }

    @Test
    void shouldDepositAndUpdateBalance() {
        // When
        BigDecimal amount = BigDecimal.valueOf(100);
        Instant transactionTime = Instant.now();

        Account account = new Account(null, BigDecimal.valueOf(1000), null);
        DepositTransaction transaction = new DepositTransaction(transactionTime, account, amount);

        AccountJpa accountJpa = new AccountJpa(BigDecimal.valueOf(1000));
        CustomerJpa customerJpa = new CustomerJpa("Salah Eddine", "AIT YOUSSEF", accountJpa);

        DepositTransactionJpa transactionJpa = new DepositTransactionJpa(transactionTime, accountJpa, amount);
        DepositStatus statusExpected = new DepositStatus(SUCCESS, amount, transactionTime);

        given(depositRepository.save(Mockito.any(DepositTransactionJpa.class))).willReturn(Mono.just(transactionJpa));
        given(customerRepository.findCustomerJpaByLastName("AIT YOUSSEF")).willReturn(Mono.just(customerJpa));
        given(mapper.map(transaction, DepositTransactionJpa.class)).willReturn(transactionJpa);
        given(accountRepository.save(accountJpa)).willReturn(Mono.just(accountJpa));

        // Act
        Mono<DepositStatus> deposit = transactionAdapter.deposit(transaction);

        // Assert
        StepVerifier.create(deposit)
                .expectNextMatches( result ->
                        result.getAmountDeposited().equals(statusExpected.getAmountDeposited()) &&
                        result.getStatusMessage().equals(statusExpected.getStatusMessage())
                )
                .expectComplete()
                .verify();
    }

    @Test
    void shouldGetCustomer (){
        AccountJpa accountJpa = new AccountJpa(BigDecimal.valueOf(1000));
        CustomerJpa customerJpa = new CustomerJpa("Salah Eddine", "AIT YOUSSEF", accountJpa);
        given(customerRepository.findCustomerJpaByLastName("AIT YOUSSEF")).willReturn(Mono.just(customerJpa));

        Mono<CustomerJpa> customerJpaMono = transactionAdapter.getCustomer.get();

        StepVerifier.create(customerJpaMono)
                .expectNextMatches( customer ->
                        customer.getFirstName().equals(customerJpa.getFirstName()) &&
                        customer.getLastName().equals(customerJpa.getLastName())
                )
                .expectComplete()
                .verify();
    }

    @Test
    void shouldWithdrawAndUpdateBalance() {
        // When
        BigDecimal amount = BigDecimal.valueOf(100);
        Instant transactionTime = Instant.now();

        Account account = new Account(null, BigDecimal.valueOf(1000), null);
        WithdrawalTransaction transaction = new WithdrawalTransaction(transactionTime, account, amount);

        AccountJpa accountJpa = new AccountJpa(BigDecimal.valueOf(1000));
        CustomerJpa customerJpa = new CustomerJpa("Salah Eddine", "AIT YOUSSEF", accountJpa);
        WithdrawalTransactionJpa transactionJpa = new WithdrawalTransactionJpa(transactionTime, accountJpa, amount);
        WithdrawalStatus statusExpected = new WithdrawalStatus(SUCCESS, amount, transactionTime);

        given(withdrawalRepository.save(Mockito.any(WithdrawalTransactionJpa.class))).willReturn(Mono.just(transactionJpa));
        given(customerRepository.findCustomerJpaByLastName("AIT YOUSSEF")).willReturn(Mono.just(customerJpa));
        given(mapper.map(transaction, WithdrawalTransactionJpa.class)).willReturn(transactionJpa);
        given(accountRepository.save(accountJpa)).willReturn(Mono.just(accountJpa));

        // Act
        Mono<WithdrawalStatus> withdraw = transactionAdapter.withdraw(transaction);

        // Assert
        StepVerifier.create(withdraw)
                .expectNextMatches( result ->
                        result.getWithdrawalAmount().equals(statusExpected.getWithdrawalAmount()) &&
                                result.getStatusMessage().equals(statusExpected.getStatusMessage())
                )
                .expectComplete()
                .verify();
    }
}