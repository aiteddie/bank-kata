package com.leanteq.bankkata.domain.use_cases;

import com.leanteq.bankkata.domain.model.*;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.math.BigDecimal;
import java.time.Instant;

import static com.leanteq.bankkata.domain.model.StatusEnum.SUCCESS;
import static org.mockito.Mockito.*;

class DepositServiceTest {

    private final TransactionPort transactionPort = mock(TransactionPort.class);

    private final DepositService subject = new DepositService(transactionPort);

    @Test
    void shouldDepositToSavingsAccount() {
        // When
        BigDecimal amount = BigDecimal.valueOf(100);
        Account account = new Account();
        Instant transactionTime = Instant.now();
        DepositTransaction transaction = new DepositTransaction(transactionTime, account, amount);
        DepositStatus statusExpected = new DepositStatus(SUCCESS, amount, transactionTime);
        doReturn(Mono.just(statusExpected)).when(transactionPort).deposit(transaction);

        // Act
        Mono<DepositStatus> depositStatus = subject.depositToSavingsAccount(transaction);

        //assert
        StepVerifier.create(depositStatus)
                .expectNextMatches( status ->
                    status.getAmountDeposited().equals(amount) &&
                    status.getStatusMessage().equals(SUCCESS) &&
                    status.getTransactionTime().equals(transactionTime)
                )
                .expectComplete()
                .verify();
    }
}