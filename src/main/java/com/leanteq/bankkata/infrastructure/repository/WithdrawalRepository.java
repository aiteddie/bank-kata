package com.leanteq.bankkata.infrastructure.repository;

import com.leanteq.bankkata.infrastructure.document.WithdrawalTransactionJpa;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * Repository interface to manage {@link WithdrawalTransactionJpa} instances.
 *
 * @author Salah Eddine AIT YOUSSEF
 */

@Repository
public interface WithdrawalRepository extends ReactiveMongoRepository<WithdrawalTransactionJpa, String> {

    /**
     * Derived query inserting {@code WithdrawalTransactionJpa}.
     *
     * @return Mono<WithdrawalTransactionJpa>
     */
    Mono<WithdrawalTransactionJpa> insert(WithdrawalTransactionJpa withdrawalTransactionJpa);
}