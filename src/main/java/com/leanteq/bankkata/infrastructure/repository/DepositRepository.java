package com.leanteq.bankkata.infrastructure.repository;

import com.leanteq.bankkata.infrastructure.document.DepositTransactionJpa;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * Repository interface to manage {@link DepositTransactionJpa} instances.
 *
 * @author Salah Eddine AIT YOUSSEF
 */

@Repository
public interface DepositRepository extends ReactiveMongoRepository<DepositTransactionJpa, String> {

    /**
     * Derived query inserting {@code  DepositTransactionJpo}.
     *
     * @return Mono<DepositTransactionJpa>
     */
    Mono<DepositTransactionJpa> insert(DepositTransactionJpa depositTransactionJpa);
}