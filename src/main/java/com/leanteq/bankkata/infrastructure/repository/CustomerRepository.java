package com.leanteq.bankkata.infrastructure.repository;

import com.leanteq.bankkata.infrastructure.document.CustomerJpa;
import com.leanteq.bankkata.infrastructure.document.DepositTransactionJpa;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

/**
 * Repository interface to manage {@link CustomerJpa} instances.
 *
 * @author Salah Eddine AIT YOUSSEF
 */
@Repository
public interface CustomerRepository extends ReactiveMongoRepository<CustomerJpa, String> {
    Mono<CustomerJpa> findCustomerJpaByLastName(String lastName);
}
