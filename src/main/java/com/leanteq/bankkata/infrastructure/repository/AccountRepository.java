package com.leanteq.bankkata.infrastructure.repository;

import com.leanteq.bankkata.infrastructure.document.AccountJpa;
import com.leanteq.bankkata.infrastructure.document.DepositTransactionJpa;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Repository interface to manage {@link AccountJpa} instances.
 *
 * @author Salah Eddine AIT YOUSSEF
 */
@Repository
public interface AccountRepository extends ReactiveMongoRepository<AccountJpa, String> {
}
