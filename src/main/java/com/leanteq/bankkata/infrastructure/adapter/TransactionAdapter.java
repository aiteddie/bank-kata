package com.leanteq.bankkata.infrastructure.adapter;

import com.leanteq.bankkata.domain.model.DepositStatus;
import com.leanteq.bankkata.domain.model.DepositTransaction;
import com.leanteq.bankkata.domain.model.WithdrawalStatus;
import com.leanteq.bankkata.domain.model.WithdrawalTransaction;
import com.leanteq.bankkata.domain.use_cases.TransactionPort;
import com.leanteq.bankkata.infrastructure.document.AccountJpa;
import com.leanteq.bankkata.infrastructure.document.CustomerJpa;
import com.leanteq.bankkata.infrastructure.document.DepositTransactionJpa;
import com.leanteq.bankkata.infrastructure.document.WithdrawalTransactionJpa;
import com.leanteq.bankkata.infrastructure.repository.AccountRepository;
import com.leanteq.bankkata.infrastructure.repository.CustomerRepository;
import com.leanteq.bankkata.infrastructure.repository.DepositRepository;
import com.leanteq.bankkata.infrastructure.repository.WithdrawalRepository;
import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Collections;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import static com.leanteq.bankkata.domain.model.StatusEnum.INSUFFICIENT_BALANCE;
import static com.leanteq.bankkata.domain.model.StatusEnum.SUCCESS;

@Service
public class TransactionAdapter implements TransactionPort {

    private DepositRepository depositRepository;

    private WithdrawalRepository withdrawalRepository;

    private CustomerRepository customerRepository;

    private AccountRepository accountRepository;

    private DozerBeanMapper mapper;
    public TransactionAdapter(){
        mapper = new DozerBeanMapper();
        mapper.setMappingFiles(Collections.singletonList("dozerJdk8Converters.xml"));
    }
    public TransactionAdapter(DepositRepository depositRepository,
                              WithdrawalRepository withdrawalRepository,
                              CustomerRepository customerRepository,
                              AccountRepository accountRepository
    ) {
        this.depositRepository = depositRepository;
        this.withdrawalRepository = withdrawalRepository;
        this.customerRepository = customerRepository;
        this.accountRepository = accountRepository;

        mapper = new DozerBeanMapper();
        mapper.setMappingFiles(Collections.singletonList("dozerJdk8Converters.xml"));
    }

    public TransactionAdapter(DepositRepository depositRepository) {
        this.depositRepository = depositRepository;
    }

    public TransactionAdapter(WithdrawalRepository withdrawalRepository) {
        this.withdrawalRepository = withdrawalRepository;
    }

    public Function<DepositTransaction, DepositTransaction> assignTransactionTime = depositTransaction -> {
        depositTransaction.setTransactionDate(Instant.now());
        return depositTransaction;
    };

    public Function<DepositTransactionJpa, Mono<DepositTransactionJpa>> insertDeposit = transaction ->
         depositRepository.save(transaction);

    public Function<WithdrawalTransactionJpa, Mono<WithdrawalTransactionJpa>> insertWithdrawal = transaction -> withdrawalRepository.save(transaction);

    public Supplier<Mono<CustomerJpa>> getCustomer = () -> customerRepository.findCustomerJpaByLastName("AIT YOUSSEF");

    public Function<DepositTransaction, DepositTransactionJpa> toDepositTransactionJpa = transaction -> {
        DepositTransactionJpa transactionJpa = mapper.map(transaction, DepositTransactionJpa.class);
        transactionJpa.setAccount(getCustomer.get().block().getAccountJpa());
        return transactionJpa;
    };

    public Function<WithdrawalTransaction, WithdrawalTransactionJpa> toWithdrawalTransactionJpa = transaction -> {
        WithdrawalTransactionJpa transactionJpa = mapper.map(transaction, WithdrawalTransactionJpa.class);
        try {
            transactionJpa.setAccountJpa(getCustomer.get().toFuture().get().getAccountJpa());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return transactionJpa;
    };

    public Function<Mono<DepositTransactionJpa>, Mono<DepositStatus>> prepareDepositResponse = response ->
            response.map(result -> new DepositStatus( SUCCESS, result.getDepositAmount(), result.getTransactionDate()));

    public Function<Mono<WithdrawalTransactionJpa>, Mono<WithdrawalStatus>> prepareWithdrawalResponse = response ->
            response.map(result -> new WithdrawalStatus( SUCCESS, result.getWithdrawalAmount(), result.getTransactionDate()));


    BiFunction<WithdrawalTransactionJpa, AccountJpa, Boolean> isAuthorized = (transaction, accountJpa) ->
            accountJpa.getBalance().compareTo(transaction.getWithdrawalAmount()) > 0;
    Consumer<AccountJpa> saveAccount = account -> accountRepository.save(account).subscribe().dispose();

    Function<Mono<WithdrawalStatus>, Mono<WithdrawalStatus>> updateBalanceAfterWithdrawal =
            withdrawalStatus -> withdrawalStatus.map(status -> {
                getCustomer.get().subscribe(customer -> {
                    AccountJpa newBalanceAccount = customer.getAccountJpa();
                    newBalanceAccount.setBalance(newBalanceAccount.getBalance().subtract(status.getWithdrawalAmount()));
                    saveAccount.accept(newBalanceAccount);
                });
                return status;
            });

    Function<Mono<DepositStatus>, Mono<DepositStatus>> updateBalanceAfterDeposit =
            withdrawalStatus -> withdrawalStatus.map(status -> {
                getCustomer.get().subscribe(customer -> {
                    AccountJpa newBalanceAccount = customer.getAccountJpa();
                    newBalanceAccount.setBalance(newBalanceAccount.getBalance().add(status.getAmountDeposited()));
                    saveAccount.accept(newBalanceAccount);
                });
                return status;
            });

    @Override
    public Mono<WithdrawalStatus> withdraw(WithdrawalTransaction transaction) throws ExecutionException, InterruptedException {
        return isAuthorized.apply(toWithdrawalTransactionJpa.apply(transaction), getCustomer.get().toFuture().get().getAccountJpa()) ?
                toWithdrawalTransactionJpa
                        .andThen(insertWithdrawal)
                        .andThen(prepareWithdrawalResponse)
                        .andThen(updateBalanceAfterWithdrawal)
                        .apply(transaction)
                : Mono.just(new WithdrawalStatus(INSUFFICIENT_BALANCE, transaction.getWithdrawalAmount(), Instant.now()));
    }
    private Mono<List<DepositTransactionJpa>> getDeposits() {
        return depositRepository.findAll().collectList();
    }

    private Mono<List<WithdrawalTransactionJpa>> getWithdrawals() {
        return withdrawalRepository.findAll().collectList();
    }

    @Override
    public Mono<Statement> getStatement(){
        Statement statement = new Statement();
        return Mono.zip(getDeposits(), getWithdrawals())
                .map(tuple2 -> statement.builder().depositTransactions(tuple2.getT1()).withdrawalTransactions(tuple2.getT2()).build());
    }

    @Override
    public Mono<DepositStatus> deposit(DepositTransaction depositTransaction) {
        return assignTransactionTime
                .andThen(toDepositTransactionJpa)
                .andThen(insertDeposit)
                .andThen(prepareDepositResponse)
                .andThen(updateBalanceAfterDeposit)
                .apply(depositTransaction);
    }
}