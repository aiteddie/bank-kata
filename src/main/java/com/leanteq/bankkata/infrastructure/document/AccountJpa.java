package com.leanteq.bankkata.infrastructure.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode

@Document(collection = "Account")
public class AccountJpa {
    @Id
    private String id;
    private BigDecimal balance;
    @DBRef
    private CustomerJpa customer;

    public AccountJpa(BigDecimal balance) {
        this.balance = balance;
    }
}