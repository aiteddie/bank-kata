package com.leanteq.bankkata.infrastructure.document;

import com.leanteq.bankkata.infrastructure.document.DepositTransactionJpa;
import com.leanteq.bankkata.infrastructure.document.WithdrawalTransactionJpa;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Statement {
    private List<WithdrawalTransactionJpa> withdrawalTransactions;
    private List<DepositTransactionJpa> depositTransactions;
}
