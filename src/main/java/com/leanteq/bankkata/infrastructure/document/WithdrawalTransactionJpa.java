package com.leanteq.bankkata.infrastructure.document;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "withdrawal_transaction")
public class WithdrawalTransactionJpa {
    @Id
    private String id;
    private Instant transactionDate;
    @DBRef
    private AccountJpa accountJpa;
    private BigDecimal withdrawalAmount;

    public WithdrawalTransactionJpa(Instant transactionDate, AccountJpa accountJpa, BigDecimal withdrawalAmount) {
        this.transactionDate = transactionDate;
        this.accountJpa = accountJpa;
        this.withdrawalAmount = withdrawalAmount;
    }
}
