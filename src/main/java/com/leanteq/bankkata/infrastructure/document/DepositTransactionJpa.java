package com.leanteq.bankkata.infrastructure.document;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "deposit_transaction")
public class DepositTransactionJpa {
    @Id
    private String id;
    private Instant transactionDate;
    @DBRef
    private AccountJpa account;
    private BigDecimal depositAmount;


    public DepositTransactionJpa(Instant transactionDate, AccountJpa account, BigDecimal depositAmount) {
        this.transactionDate = transactionDate;
        this.account = account;
        this.depositAmount = depositAmount;
    }
}