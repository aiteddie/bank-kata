package com.leanteq.bankkata.infrastructure.document;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@EqualsAndHashCode
@Document(collection = "Customer")
public class CustomerJpa {
    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String address;
    private String email;
    @DBRef
    private AccountJpa accountJpa;

    public CustomerJpa(String firstName, String lastName, AccountJpa accountJpa) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountJpa = accountJpa;
    }
}