package com.leanteq.bankkata.domain.model;

public enum StatusEnum {
    SUCCESS, PENDING, FAILED, INSUFFICIENT_BALANCE
}