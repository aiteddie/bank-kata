package com.leanteq.bankkata.domain.model;

import java.math.BigDecimal;
import java.time.Instant;

public class WithdrawalTransaction extends Transaction {
    private BigDecimal withdrawalAmount;

    public WithdrawalTransaction(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    public WithdrawalTransaction(Instant transactionDate, Account account, BigDecimal withdrawalAmount) {
        super(transactionDate, account);
        this.withdrawalAmount = withdrawalAmount;
    }

    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }
}