package com.leanteq.bankkata.domain.model;

import java.math.BigDecimal;
import java.time.Instant;

public class DepositStatus {
    private StatusEnum statusMessage;
    private BigDecimal amountDeposited;
    private Instant transactionTime;

    public DepositStatus() {
    }


    public DepositStatus(StatusEnum statusMessage, BigDecimal amountDeposited, Instant transactionTime) {
        this.statusMessage = statusMessage;
        this.amountDeposited = amountDeposited;
        this.transactionTime = transactionTime;
    }

    public StatusEnum getStatusMessage() {
        return statusMessage;
    }

    public BigDecimal getAmountDeposited() {
        return amountDeposited;
    }

    public Instant getTransactionTime() {
        return transactionTime;
    }

    public void setStatusMessage(StatusEnum statusMessage) {
        this.statusMessage = statusMessage;
    }

    public void setAmountDeposited(BigDecimal amountDeposited) {
        this.amountDeposited = amountDeposited;
    }

    public void setTransactionTime(Instant transactionTime) {
        this.transactionTime = transactionTime;
    }
}
