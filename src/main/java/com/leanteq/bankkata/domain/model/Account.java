package com.leanteq.bankkata.domain.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

public class Account {
    private Long id;
    private BigDecimal balance;
    private Customer customer;
    Collection<Transaction> transactions = new ArrayList<>();


    public Account() {
    }

    public Account(Long id, BigDecimal balance, Customer customer) {
        this.id = id;
        this.balance = balance;
        this.customer = customer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}