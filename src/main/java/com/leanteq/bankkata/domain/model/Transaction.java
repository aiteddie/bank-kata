package com.leanteq.bankkata.domain.model;

import java.time.Instant;

public class Transaction {
    private String id;
    private Instant transactionDate;
    private Account account;

    public Transaction() {
    }

    public Transaction(Instant transactionDate, Account account) {
        this.transactionDate = transactionDate;
        this.account = account;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Instant getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Instant transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}