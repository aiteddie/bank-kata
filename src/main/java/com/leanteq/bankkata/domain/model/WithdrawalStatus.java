package com.leanteq.bankkata.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WithdrawalStatus {
    private StatusEnum statusMessage;
    private BigDecimal withdrawalAmount;
    private Instant transactionTime;
}