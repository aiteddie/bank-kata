package com.leanteq.bankkata.domain.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class DepositTransaction extends Transaction {

    private BigDecimal depositAmount;

    public DepositTransaction(Instant transactionDate, Account account, BigDecimal depositAmount) {
        super(transactionDate, account);
        this.depositAmount = depositAmount;
    }

    public DepositTransaction(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepositTransaction that = (DepositTransaction) o;
        return Objects.equals(getDepositAmount(), that.getDepositAmount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDepositAmount());
    }
}
