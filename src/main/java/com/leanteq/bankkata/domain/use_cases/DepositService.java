package com.leanteq.bankkata.domain.use_cases;

import com.leanteq.bankkata.domain.model.DepositStatus;
import com.leanteq.bankkata.domain.model.DepositTransaction;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class DepositService implements DepositServiceInterface{

    private TransactionPort transactionPort;

    public DepositService(TransactionPort transactionPort) {
        this.transactionPort = transactionPort;
    }

    public Mono<DepositStatus> depositToSavingsAccount (DepositTransaction transaction) {
        return transactionPort.deposit(transaction);
    }
}