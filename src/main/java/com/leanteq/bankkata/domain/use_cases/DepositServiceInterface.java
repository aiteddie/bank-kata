package com.leanteq.bankkata.domain.use_cases;

import com.leanteq.bankkata.domain.model.DepositStatus;
import com.leanteq.bankkata.domain.model.DepositTransaction;
import reactor.core.publisher.Mono;

public interface DepositServiceInterface {
     Mono<DepositStatus> depositToSavingsAccount (DepositTransaction transaction);
}