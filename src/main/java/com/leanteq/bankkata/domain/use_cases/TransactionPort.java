package com.leanteq.bankkata.domain.use_cases;

import com.leanteq.bankkata.domain.model.DepositStatus;
import com.leanteq.bankkata.domain.model.DepositTransaction;
import com.leanteq.bankkata.domain.model.WithdrawalStatus;
import com.leanteq.bankkata.domain.model.WithdrawalTransaction;
import com.leanteq.bankkata.infrastructure.document.Statement;
import reactor.core.publisher.Mono;

public interface TransactionPort {
    Mono<DepositStatus>      deposit     (DepositTransaction depositTransaction);

    Mono<WithdrawalStatus>   withdraw    (WithdrawalTransaction withdrawalTransaction) throws ExecutionException, InterruptedException;

    Mono<Statement>   getStatement();

}