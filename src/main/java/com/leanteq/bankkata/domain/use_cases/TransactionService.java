package com.leanteq.bankkata.domain.use_cases;

import com.leanteq.bankkata.domain.model.Statement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class TransactionService {
    @Autowired
    private TransactionPort transactionPort;

    public TransactionService(TransactionPort transactionPort) {
        this.transactionPort = transactionPort;
    }

    public TransactionService() {
    }

    public Mono<Statement> getTransactions () {
        return transactionPort.getStatement();
    }

}