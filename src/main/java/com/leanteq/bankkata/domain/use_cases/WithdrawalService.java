package com.leanteq.bankkata.domain.use_cases;

import com.leanteq.bankkata.domain.model.WithdrawalStatus;
import com.leanteq.bankkata.domain.model.WithdrawalTransaction;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class WithdrawalService {

    private TransactionPort transactionPort;

    public WithdrawalService(TransactionPort transactionPort) {
        this.transactionPort = transactionPort;
    }

    public Mono<WithdrawalStatus> withdraw (WithdrawalTransaction transaction) {
        return transactionPort.withdraw(transaction);
    }

}