package com.leanteq.bankkata.application;

import com.leanteq.bankkata.domain.model.WithdrawalStatus;
import com.leanteq.bankkata.domain.model.WithdrawalTransaction;
import com.leanteq.bankkata.domain.use_cases.WithdrawalService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/withdraw")
public class WithdrawController {

    private final WithdrawalService withdrawalService;

    public WithdrawController(WithdrawalService withdrawalService) {
        this.withdrawalService = withdrawalService;
    }

    @PostMapping(produces = "application/json")
    Mono<WithdrawalStatus> withdraw (@RequestBody WithdrawalTransaction transaction) {
        return this.withdrawalService.withdraw(transaction);
    }
}
