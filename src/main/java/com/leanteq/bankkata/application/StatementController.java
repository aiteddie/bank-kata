package com.leanteq.bankkata.application;

import com.leanteq.bankkata.domain.model.DepositStatus;
import com.leanteq.bankkata.domain.model.DepositTransaction;
import com.leanteq.bankkata.domain.model.Statement;
import com.leanteq.bankkata.domain.use_cases.DepositService;
import com.leanteq.bankkata.domain.use_cases.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api/v1/statement")
public class StatementController {
    @Autowired
    private final TransactionService transactionService;

    public StatementController(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @GetMapping(produces = "application/json")
    Mono<Statement> getStatement(){
        return transactionService.getTransactions();
    }
}
