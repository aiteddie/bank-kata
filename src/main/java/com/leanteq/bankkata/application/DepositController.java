package com.leanteq.bankkata.application;

import com.leanteq.bankkata.domain.model.DepositStatus;
import com.leanteq.bankkata.domain.model.DepositTransaction;
import com.leanteq.bankkata.domain.use_cases.DepositService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * Deposit Controller class.
 *
 * @author Salah Eddine AIT YOUSSEF
 */

@RestController
@RequestMapping("/api/v1/deposit")
public class DepositController {

    private final DepositService depositService;

    public DepositController(DepositService depositService) {
        this.depositService = depositService;
    }

    @PostMapping(produces = "application/json")
    Mono<DepositStatus> deposit(@RequestBody DepositTransaction transaction){
        return depositService.depositToSavingsAccount(transaction);
    }
}